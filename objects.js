function Point(x=0, y=0) {
    Object.defineProperties(this, {
        "x": {
            value: x,
            writable: false,
            enumerable: true,
            configurable: false
        },
        "y": {
            value: y,
            writable: false,
            enumerable: true,
            configurable: false
        }
    });
}

Point.prototype.average = function(p2, alpha) {
    "use strict";
    if (alpha < 0) {
        alpha = 0;
    } else if (alpha > 1000) {
        alpha = 1000;
    }
    var n_x = (1 - alpha / 1000) * this.x + alpha / 1000 * p2.x;
    var n_y = (1 - alpha / 1000) * this.y + alpha / 1000 * p2.y;
    return (new Point(n_x, n_y));
};

Point.prototype.symetryV = function (n) {
    "use strict";
    var n_x = this.x + 2 * (n - this.x);
    return (new Point(n_x, this.y));
};

Point.prototype.symetryH = function (n) {
    "use strict";
    var n_y = this.y + 2 * (n - this.y);
    return (new Point(this.x, n_y));
};

Point.prototype.clone = function() {
    "use strict";
    return (new Point(this.x, this.y));
};


function Segment(p1=null, p2=null, color="rgba(255, 255, 255, 1)") {
    Object.defineProperties(this, {
        "p1": {
            value: p1,
            writable: false,
            enumerable: true,
            configurable: false
        },
        "p2": {
            value: p2,
            writable: false,
            enumerable: true,
            configurable: false
        },
        "color": {
            value: color,
            writable: false,
            enumerable: true,
            configurable: false
        }
    });
}

Segment.prototype.symetryH = function (n) {
    "use strict";
    return (new Segment(this.p1.symetryH(n), this.p2.symetryH(n), this.color));
};

Segment.prototype.symetryV = function (n) {
    "use strict";
    return (new Segment(this.p1.symetryV(n), this.p2.symetryV(n), this.color));
};

Segment.prototype.draw = function(ctx) {
    "use strict";
    ctx.strokeStyle = this.color;
    ctx.beginPath();
    ctx.moveTo(this.p1.x, this.p1.y);
    ctx.lineTo(this.p2.x, this.p2.y);
    ctx.stroke();
};

Segment.prototype.average = function(seg2, alpha) {
    "use strict";
    var n_p1 = this.p1.average(seg2.p1, alpha);
    var n_p2 = this.p2.average(seg2.p2, alpha);
    var n_color = getColorAverage(this.color, seg2.color, alpha);
    return (new Segment(n_p1, n_p2, n_color));
};


function Figure() {
    "use strict";
    this.list_seg = [];
    this.lastPoint = null;
}

Figure.prototype.symetryH = function (n) {
    "use strict";
    var i;
    var res = new Figure();
    for (i = 0; i < this.list_seg.length; i += 1) {
        res.list_seg.push(this.list_seg[i].symetryH(n));
    }
    return (res);
};

Figure.prototype.symetryV = function (n) {
    "use strict";
    var i;
    var res = new Figure();
    for (i = 0; i < this.list_seg.length; i += 1) {
        res.list_seg.push(this.list_seg[i].symetryV(n));
    }
    return (res);
};

Figure.prototype.handleClick = function (pt, shiftKey, color) {
    "use strict";
    if (this.lastPoint === null || shiftKey) {
        this.lastPoint = pt;
    } else {
        if (isRGBA(color)) {
            this.list_seg.push(new Segment(this.lastPoint, pt, color));
        } else {
            this.list_seg.push(new Segment(this.lastPoint, pt));
        }
        this.lastPoint = pt;
    }
};

Figure.prototype.draw = function (ctx, canvasRect) {
    "use strict";
    clearCanvas(ctx, canvasRect);
    var i;
    for (i = 0; i < this.list_seg.length; i += 1) {
        this.list_seg[i].draw(ctx);
    }
};

Figure.prototype.average = function (figure, alpha) {
    "use strict";
    var res = new Figure();
    var i;
    var n_seg;
    for (i = 0; i < this.list_seg.length; i += 1) {
        n_seg = this.list_seg[i].average(figure.list_seg[i], alpha);
        res.list_seg.push(n_seg);
    }
    return (res);
};

Figure.prototype.clone = function() {
    "use strict";
    var res = new Figure();
    var i;
    for (i = 0; i < this.list_seg.length; i += 1) {
        res.list_seg.push(this.list_seg[i]);
    }
    return (res);
};


function Model () {
    "use strict";
    this.list_fig = [];
    this.c_fig = 0;
    this.alpha = 0;
    this.al_inc = 8;
}

Model.prototype.append = function(figure) {
    "use strict";
    var fig = figure.clone();
    if (this.list_fig.length !== 0) {
        var i;
        for (i = 0; i < this.list_fig.length; i += 1) {
            equalize(this.list_fig[i], fig);
        }
    }
    this.list_fig.push(fig);
};

Model.prototype.draw = function(ctx, canvasRect) {
    "use strict";
    if (this.list_fig.length < 2) {
        return (1);
    }
    var n_fig = this.list_fig[this.c_fig].average(this.list_fig[this.c_fig + 1], this.alpha);
    this.alpha += this.al_inc;
    if (this.alpha >= 1100) {
        if (this.c_fig < this.list_fig.length - 2) {
            this.c_fig += 1;
            this.alpha = 0;
        } else {
            this.al_inc *= -1;
        }
    }
    if (this.alpha < -100) {
        if (this.c_fig > 0) {
            this.c_fig -= 1;
            this.alpha = 1000;
        } else {
            this.al_inc *= -1;
        }
    }
    n_fig.draw(ctx, canvasRect);
    return (0);
};
