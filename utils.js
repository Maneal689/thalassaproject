function clearCanvas(ctx, canvasRect) {
    "use strict";
    ctx.fillStyle = "#FFF";
    //ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvasRect.width, canvasRect.height);
}

function myRandRange(n) {
    "use strict";
    return (Math.floor(Math.random() * n));
}

function isRGBA(str) {
    "use strict";
    var reg = /^rgba\(([0-9]{1,3},\s?){3}[0-9.]{1,}\)$/;
    return (reg.test(str));
}

function getRgba(str) {
    "use strict";
    if (!isRGBA(str)) {
        return (null);
    }
    var reg = /[0-9.]+/g;
    return (str.match(reg));
}

function getColorAverage(c1, c2, alpha) {
    "use strict";
    var coef = alpha / 1000;
    var res = "rgba(";
    c1 = getRgba(c1);
    c2 = getRgba(c2);
    res += ((1 - coef) * c1[0] + coef * c2[0]);
    res += ",";
    res += ((1 - coef) * c1[1] + coef * c2[1]);
    res += ",";
    res += ((1 - coef) * c1[2] + coef * c2[2]);
    res += ",";
    res += ((1 - coef) * c1[3] + coef * c2[3]);
    res += ")";
    return (res);
}

function cutSeg(seg) {
    "use strict";
    var pMid = new Point((seg.p1.x + seg.p2.x) / 2, (seg.p1.y + seg.p2.y) / 2);
    var seg1 = new Segment(seg.p1, pMid, seg.color);
    var seg2 = new Segment(pMid, seg.p2, seg.color);
    return ([seg1, seg2]);
}

function equalize(fig1, fig2) {
    "use strict";
    var l1 = fig1.list_seg.length;
    var l2 = fig2.list_seg.length;
    if (l1 !== l2) {
        if (l1 < l2) {
            var toCut, n_segs, to_cpy;
            while (l1 < l2) {
                if (l1 > 0) {
                    toCut = myRandRange(l1);
                    n_segs = cutSeg(fig1.list_seg[toCut]);
                    fig1.list_seg.splice(toCut, 1, n_segs[0]);
                    fig1.list_seg.splice(toCut + 1, 0, n_segs[1]);
                } else {
                    to_cpy = fig2.list_seg[myRandRange(l2)];
                    fig1.list_seg.push(new Segment(to_cpy.p1, to_cpy.p1));
                }
                l1 += 1;
            }
        }
        else {
            equalize(fig2, fig1);
        }
    }
}

function parseJsonSeg(obj) {
    "use strict";
    var p1 = new Point(obj.p1.x, obj.p1.y);
    var p2 = new Point(obj.p2.x, obj.p2.y);
    return (new Segment(p1, p2, obj.color));
}

function parseJsonFig(obj) {
    "use strict";
    var res = new Figure();
    var i;
    for (i = 0; i < obj.list_seg.length; i += 1) {
        res.list_seg.push(parseJsonSeg(obj.list_seg[i]));
    }
    return (res);
}

function isNbr(str) {
    "use strict";
    var reg = /^[0-9.]+$/;
    return (reg.test(str));
}

function updateColorBg(domElm) {
    "use strict";
    if (isRGBA(domElm.value)) {
        domElm.style.backgroundColor = domElm.value;
        var rgba = getRgba(domElm.value);
        var rgb = rgba.slice(0, 3).map(function(elm) {return (parseInt(elm));});
        var moy = rgb.reduce(function(cpt, elm) {return (cpt + elm);}, 0) / 3;
        if (moy < 127.5) {
            domElm.style.color = "white";
        } else {
            domElm.style.color = "black";
        }
    }
}

function swichForm (form){
    "use strict";
    if (form.style.left === "" || form.style.left === "0px") {
        form.style.left = "-460px";
    } else {
        form.style.left = "";
    }
}
