window.onload = function() {
    "use strict";
    var canvas = document.getElementById("canvas");
    var canvasRect = canvas.getBoundingClientRect();
    var ctx = canvas.getContext("2d");
    clearCanvas(ctx, canvasRect);

    var editText = document.getElementById("editText");
    var jsonText = document.getElementById("jsonCode");
    jsonText.style.resize = "none";
    var nextButton = document.getElementById("nextButton");
    var previousButton = document.getElementById("previousButton");
    var nextButton2 = document.getElementById("nextButton2");
    var previousButton2 = document.getElementById("previousButton2");
    var swichButton = document.getElementById("swich");
    var importButton = document.getElementById("importButton");
    var undoButton = document.getElementById("undoButton");
    var clearButton = document.getElementById("clearButton");
    var formSwich = document.getElementById("formSwich");
    var symVButton = document.getElementById("symVButton");
    var symHButton = document.getElementById("symHButton");

    var color = document.getElementById("color");
    var speedText = document.getElementById("speed");

    var selectFig = document.getElementById("selectFig");
    var pasteButton = document.getElementById("pasteButton");

    var form = document.getElementById("form");
    form.onsubmit = function () {
        return (false);
    };

    var editObj = {
        figs: [],
        cEditFig: 0,
        running: false,
        get figure() {
            return (this.figs[this.cEditFig]);
        }
    };
    editObj.figs.push(new Figure());
    jsonText.value = JSON.stringify(editObj.figure);

    var modele = null;
    var idProcess = null;
    var frameRate = 25;

    var upColorId = window.setInterval(function () {updateColorBg(color);}, 1000 / frameRate);


    symHButton.onclick = function() {
        if (!editObj.running) {
            editObj.figs[editObj.cEditFig] = editObj.figure.symetryH(300);
            editObj.figure.draw(ctx, canvasRect);
        }
    };

    symVButton.onclick = function() {
        if (!editObj.running) {
            editObj.figs[editObj.cEditFig] = editObj.figure.symetryV(650);
            editObj.figure.draw(ctx, canvasRect);
        }
    };

    formSwich.onclick = function () {
        swichForm(form);
        formSwich.textContent = formSwich.textContent === "<" ? ">" : "<";
    };
    clearButton.onclick = function () {
        if (!editObj.running) {
            clearCanvas(ctx, canvasRect);
            editObj.figs[editObj.cEditFig] = new Figure();
            jsonText.value = JSON.stringify(editObj.figure);
        }
    };
    undoButton.onclick = function () {
        if (!editObj.running) {
            editObj.figure.list_seg.pop();
            if (editObj.figure.list_seg.length > 0) {
                editObj.figure.lastPoint = editObj.figure.list_seg[editObj.figure.list_seg.length - 1].p2;
            } else {
                editObj.figure.lastPoint = null;
            }
            editObj.figure.draw(ctx, canvasRect);
            jsonText.value = JSON.stringify(editObj.figure);
        }
    };
    pasteButton.onclick = function () {
        jsonText.value = JSON.stringify(saved_fig[selectFig.value]);
    };
    swichButton.onclick = function () {
        if (this.textContent === "Start" && editObj.figs.length > 1) {
            var i;
            modele = new Model();
            var secPerFigure = isNbr(speedText.value) ? parseFloat(speedText.value) : 3;
            modele.al_inc = 1000 / secPerFigure / frameRate;
            for (i = 0; i < editObj.figs.length; i += 1) {
                modele.append(editObj.figs[i]);
            }
            this.textContent = "Stop";
            editObj.running = true;
            editText.textContent = "Playing...";
            idProcess = window.setInterval(function () {modele.draw(ctx, canvasRect);}, 1000 / frameRate);
        }
        else {
            if (idProcess) {
                editObj.figure.draw(ctx, canvasRect);
                editObj.running = false;
                this.textContent = "Start";
                clearInterval(idProcess);
                editText.textContent = "Editing figure n° " + (editObj.cEditFig + 1);
            }
        }
    };
    importButton.onclick = function () {
        if (!editObj.running) {
            editObj.figs[editObj.cEditFig] = parseJsonFig(JSON.parse(jsonText.value));
            editObj.figure.draw(ctx, canvasRect);
            jsonText.value = JSON.stringify(editObj.figure);
        }
    };
    nextButton.onclick = function () {
        if (!editObj.running) {
            editObj.cEditFig += 1;
            if (editObj.cEditFig >= editObj.figs.length) {
                editObj.figs.push(new Figure());
            }
            editText.textContent = "Editing figure n° " + (editObj.cEditFig + 1);
            jsonText.value = JSON.stringify(editObj.figure);
            editObj.figure.draw(ctx, canvasRect);
        }
    };
    nextButton2.onclick = nextButton.onclick;
    previousButton.onclick = function () {
        if (!editObj.running) {
            if (editObj.cEditFig >= 1) {
                //Vide les dernieres figures si elles sont vide.
                if (editObj.cEditFig === editObj.figs.length - 1 && editObj.figure.list_seg.length === 0) {
                    editObj.figs.splice(editObj.cEditFig, 1);
                }
                editObj.cEditFig -= 1;
            }
            editText.textContent = "Editing figure n° " + (editObj.cEditFig + 1);
            jsonText.value = JSON.stringify(editObj.figure);
            editObj.figure.draw(ctx, canvasRect);
        }
    };
    previousButton2.onclick = previousButton.onclick;
    canvas.onclick = function(e) {
        if (!editObj.running) {
            var clickX = Math.floor(e.x - canvasRect.left);
            var clickY = Math.floor(e.y - canvasRect.top);
//+ window.scrollY
            editObj.figure.handleClick(new Point(clickX, clickY), e.shiftKey, color.value);
            jsonText.value = JSON.stringify(editObj.figure);
            editObj.figure.draw(ctx, canvasRect);
        }
    };
};
